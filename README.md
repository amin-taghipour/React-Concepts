# React-Concepts

Each branch one lesson

1. [Thinking in react](https://gitlab.com/amin-taghipour/React-Concepts/-/tree/thinking-in-react)
2. [Describing the UI](https://gitlab.com/amin-taghipour/React-Concepts/-/tree/describing-the-ui)
3. [Adding interactivity](https://gitlab.com/amin-taghipour/React-Concepts/-/tree/adding-interactivity)
